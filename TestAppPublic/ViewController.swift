//
//  ViewController.swift
//  TestAppPublic
//
//  Created by Alex Gold on 31/08/2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .yellow
    }
}

